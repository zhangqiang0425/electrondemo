/*
菜单类型:
1.normal:默认的菜单类型
2.separator:分割线
3.submenu:子菜单
4.checkbox:多选菜单
5.radio:多选菜单

为菜单项增加图标
window:ico格式  16*16
其他系统:png格式  16*16
使用icon设置图标,图标会按照原始尺寸显示，所以尽量选择合适的尺寸

动态创建菜单

上下文菜单


* */

const electron = require('electron');
const app = electron.app;
const BrowserWindow = electron.BrowserWindow;
const Menu = electron.Menu;
function createWindow () {
  win = new BrowserWindow({width:800,height:600,webPreferences:{
    nodeIntegration :true
    }});
  win.loadFile('index.html');

  //win.webContents.openDevTools();
  win.on('closed',()=>{
    console.log('closed');
    win == null;
  })
}
app.on('ready', createWindow);
app.on('window-all-closed', ()=>{
  console.log('window-all-closed');
  if(process.platform != 'darwin'){
    app.quit();
  }
});
app.on('activate',()=>{
  console.log('activate');
  if(win == null){
    createWindow();
  }
});
