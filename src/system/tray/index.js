/*
托盘应用

添加多个托盘图标

托盘事件



* */

const {app,Menu,Tray,BrowserWindow} = require('electron');

let tray;
let contextMenu;
function createWindow () {
  win = new BrowserWindow({width:800,height:600,webPreferences:{
    nodeIntegration :true
    }});
  var icon = '..//..//..//image/12.png';
  win.loadFile('index.html');
  win.setIcon(icon);
  //添加托盘图标
  tray = new Tray('..//..//..//image/12.png');
  //为托盘图标添加上下文菜单
  const template = [{label:'复制', role:'copy'},{label:'粘贴', role:'paste'},{label:'剪切', role:'cut'}];

  contextMenu = Menu.buildFromTemplate(template);
  tray.setToolTip('诸城联合奖惩');
  tray.setContextMenu(contextMenu);

  //win.webContents.openDevTools();
  win.on('closed',()=>{
    console.log('closed');
    win == null;
  })
}
app.on('ready', createWindow);
app.on('window-all-closed', ()=>{
  console.log('window-all-closed');
  if(process.platform != 'darwin'){
    app.quit();
  }
});
app.on('activate',()=>{
  console.log('activate');
  if(win == null){
    createWindow();
  }
});
