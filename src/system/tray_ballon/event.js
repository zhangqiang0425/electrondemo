const electron = require('electron');
const app = electron.app;
const remote = electron.remote;
const BrowserWindow = remote.BrowserWindow;
const Menu = remote.Menu;
const MenuItem = remote.MenuItem;
const Tray = remote.Tray;

let tray;
let contextMenu;

function onClick_AddTray(){
    if(tray != undefined){
        return;
    }
    //添加托盘图标
    tray = new Tray('..//..//..//image/12.png');
    //为托盘图标添加上下文菜单
    const template = [{label:'复制', role:'copy'},{label:'粘贴', role:'paste'},{label:'剪切', role:'cut'}];

    contextMenu = Menu.buildFromTemplate(template);
    tray.setToolTip('诸城联合奖惩');
    tray.setContextMenu(contextMenu);

    tray.on('left-click',(event)=>{
        textarea.value += '\r\n' + 'left-click';
        if(event.shiftKey){
            window.open('https://www.baidu.com','left-click','width=300,height=200');
        }else{
            tray.popUpContextMenu(contextMenu);
        }
    });

    //单击事件
    tray.on('click',(event)=>{
        textarea.value += '\r\n' + 'click';
        if(event.shiftKey){
            window.open('https://www.jd.com','click','width=300,height=200');
        }else{
            tray.popUpContextMenu(contextMenu);
        }
    });
    //气泡显示事件
    tray.on('balloon-show',()=>{
        log.value += 'balloon-show\r\n';
    });
    //气泡单击事件
    tray.on('balloon-click',()=>{
        log.value += 'balloon-click\r\n';
    });
    //气泡关闭事件
    tray.on('balloon-closed',()=>{
        log.value += 'balloon-closed\r\n';
    });
}

//设置托盘
function onClick_SetIcon(){
    if(tray != undefined){
        tray.setImage('..//..//..//image/12.png');
    }
}
//设置托盘的显示文本
function onClick_SetToolTip(){
    if(tray != undefined){
        tray.setToolTip('哈哈哈哈');
    }
}
//移除托盘
function onClick_RemoveTray(){
    if(tray != undefined){
        tray.destroy();
        tray = undefined;
    }
}
//气泡
function onClick_DisplayBallon(){
    if(tray != undefined){
        tray.displayBalloon({title:'有消息了',icon:'..//..//..//image/12.png',content:'软件更新了，\r\n 请重新下载!'});
    }
}
