/*
托盘应用

添加多个托盘图标

托盘事件
altKey:Alt键
shiftKey:Shift键
ctrlKey:Ctrl键
metaKey:Meta键 在Mac OS X下是Command键，windows下是窗口键

* */

const {app,Menu,Tray,BrowserWindow} = require('electron');


function createWindow () {
  win = new BrowserWindow({width:800,height:600,webPreferences:{
    nodeIntegration :true
    }});
  var icon = '..//..//..//image/12.png';
  win.loadFile('index.html');
  win.setIcon(icon);

  //win.webContents.openDevTools();
  win.on('closed',()=>{
    console.log('closed');
    win == null;
  })
}
app.on('ready', createWindow);
app.on('window-all-closed', ()=>{
  console.log('window-all-closed');
  if(process.platform != 'darwin'){
    app.quit();
  }
});
app.on('activate',()=>{
  console.log('activate');
  if(win == null){
    createWindow();
  }
});
