/*
托盘应用

添加多个托盘图标

托盘事件
altKey:Alt键
shiftKey:Shift键
ctrlKey:Ctrl键
metaKey:Meta键 在Mac OS X下是Command键，windows下是窗口键

托盘方法
tray.setImage('..//..//..//image/12.png');设置托盘图标
tray.setToolTip('哈哈哈哈');//设置托盘的显示文本
tray.setTitle('hello world');//设置托盘标题(仅限Mac OS)
tray.setPressedImage('..//..//..//image/12.png');//设置托盘按下后显示的图标(仅限Mac OS)
tray.destroy();移除托盘

气泡事件
balloon-show 气泡显示事件
balloon-click 气泡单击事件
balloon-closed 气泡关闭事件
气泡的click和closed事件是互斥的，单击气泡只会触发click事件，当气泡自动关闭后，会触发closed事件
* */

const {app,Menu,Tray,BrowserWindow} = require('electron');


function createWindow () {
  win = new BrowserWindow({width:800,height:600,webPreferences:{
    nodeIntegration :true
    }});
  //var icon = '..//..//..//image/12.png';
  win.loadFile('index.html');
  //win.setIcon(icon);

  //win.webContents.openDevTools();
  win.on('closed',()=>{
    console.log('closed');
    win == null;
  })
}
app.on('ready', createWindow);
app.on('window-all-closed', ()=>{
  console.log('window-all-closed');
  if(process.platform != 'darwin'){
    app.quit();
  }
});
app.on('activate',()=>{
  console.log('activate');
  if(win == null){
    createWindow();
  }
});
