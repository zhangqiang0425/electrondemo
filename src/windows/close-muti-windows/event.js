const remote = require('electron').remote;
const BrowserWindow = remote.BrowserWindow;
//关闭当前窗口
function onClick_Close(){
    const win = remote.getCurrentWindow();
    win.close();
}
//创建多个窗口
function onclick_Create_MutiWindows(){
    if(global.windows == undefined){
        //初始化windows数组
        global.windows = [];
    }
    //win = new BrowserWindow({show:false,x:10,y:20,width:200,height:300});
    var win = new BrowserWindow({show:false,x:10,y:20,width:200,height:300,webPreferences : {
            nodeIntegration: true
        }});
    global.windows.push(win);
    win.loadFile('child.html');
    win.on('ready-to-show',()=>{
        win.show();
    });
}
//关闭所有窗口(除了主窗口)
function onclick_Close_MutiWindows(){
    if(global.windows != undefined){
        //扫描并关闭除了主窗口外的所有窗口
        for(var i =0; i < global.windows.length; i++){
            global.windows[i].close();
        }
        global.windows.length =0;
        global.windows = undefined;
    }
}




