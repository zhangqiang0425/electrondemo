//窗口的尺寸和位置
/*
* width:窗口宽度
* height:窗口高度
*
* minWidth:窗口允许的最小宽度
* minHeight:窗口允许的最小高度
* maxWidth:窗口允许的最大宽度
* maxHeight:窗口允许的最大高度
*
* x:指定窗口的横坐标
* y:指定窗口的纵坐标
*
* 获取窗口尺寸
* getSize() 返回数组，[0]：width   [1]:height
* 设置窗口尺寸
* setSize(width,height,flag) flag：true,以动画效果改变尺寸(仅限于Mac Os)
* 获取窗口位置
* getPosition() 返回数组，[0]：x   [1]:y
* 设置窗口位置
* setPosition(x,y,flag) flag：true,以动画效果改变位置(仅限于Mac Os)
*
* 全屏窗口
* fullscreen:true
* 如果设置了width，height、x、y，系统会忽略这些属性，仍然是全屏显示
* maxWidth、maxHeight
*
* fullscreenable:false,在Mac OS X会阻止单击最大化按钮隐藏菜单
*
* setFullScreen方法可以动态将窗口设置为全屏状态，但fullscreenable属性值必须为true
*
* 通过win.isFullScreen()方法可以获取窗口是否为全屏
*
* 无边框窗口和透明窗口
* frame:false
* transparent:true
*
* 锁定模式
* 如果窗口处于全屏，并且锁定状态，在Mac OS X唯一退出窗口的方式是通过Command+Q组合键退出
* 如果在Mac OS X下，使用setKiosk方法切换窗口的锁定模式，不能将fullscreen属性设置为true
*
* require is not defined的问题解决是设置属性webPreferences.nodeIntegration为 true
* let win = new BrowserWindow({
    webPreferences: {
        nodeIntegration: true
    }
})
*
*
* 窗口图标
* icon setIcon
*
*优雅装载页面
* 1.创建一个隐藏窗口
* 2.装载页面
* 3.将ready-to-show事件绑定到窗口上
* 4.在ready-to-show事件中显示窗口
*
*父子窗口
*1.子窗口总是在父窗口之上
* 2.如果父窗口关闭，子窗口自动关闭
* 子窗口相当于父窗口的悬浮窗口
*Mac OS X和Windows的父子窗口的区别是在Mac OS X下，移动父窗口，子窗口随着父窗口移动，在Windows下子窗口不会随着父窗口移动
*
*模态窗口
* 模态窗口是指禁用父窗口的子窗口，也就是处于模态的子窗口显示后，无法使用父窗口，知道子窗口关闭
* 1.模态窗口需要是另外一个窗口的子窗口
* 2.一旦模态窗口显示，父窗口将无法使用
* modal = true
* Mac OS X和WIndows的模态窗口差异
* 1.在Mac OS X下会隐藏窗口的标题栏，只能通过close方法关闭模态子窗口
*   在Windows下，模态子窗口仍然会显示菜单和标题栏
* 2.在Mac OS X下，模态子窗口显示后，父窗口仍然可以拖动，但无法关闭
*   在Windows下，模态子窗口显示后父窗口无法拖动
*应用:主要用于桌面应用的对话框显示，例如:设置对话框、打开对话框
*
*关闭多个窗口
* BrowserWindow对象的close方法用于关闭当前窗口
* 关闭多窗口的基本原理:将所有窗口的BrowserWindow对象保存起来，只需要调用指定窗口的close方法，
* 就可以关闭指定的一些窗口
*global：全局变量，将所有窗口的BrowserWindow对象保存到windows数组中，将该数组保存到global中
*
*
*
*
* */

const {app, BrowserWindow} = require('electron');
function createWindow () {
  win = new BrowserWindow({show:false,webPreferences : {
      nodeIntegration: true
    }});
  win.loadFile('index.html');
  win.on('ready-to-show',()=>{
    win.show();
  });
  win.on('closed',()=>{
    console.log('closed');
    win == null;
  });
}
app.on('ready', createWindow);
app.on('window-all-closed', ()=>{
  console.log('window-all-closed');
  if(process.platform != 'darwin'){
    app.quit();
  }
});
app.on('activate',()=>{
  console.log('activate');
  if(win == null){
    createWindow();
  }
});
