const remote = require('electron').remote;
const dialog = remote.dialog;

const {ipcRenderer} = require('electron');
const ipcMain = remote.ipcMain;
ipcMain.on('close',(event,str)=>{
    alert(str);
});
//打开子窗口
function onClick_openWindow(){
   win =  window.open('child.html','','width = 300,height = 200');
}
//获取焦点
function onClick_focus(){
    if(win !=undefined){
        win.focus();
    }
}
//失去焦点
function onClick_blur(){
    if(win !=undefined){
        win.blur();
    }
}
//关闭窗口
function onClick_close(){
    if(win != undefined){
        if(win.closed){
            alert('子窗口已经关闭，不需要重复关闭!');
            return ;
        }
        win.close();
    }
}
//显示打印窗口
function onClick_print(){
    if(win !=undefined){
        win.print();
    }
}

//向子页面传递数据
function onClick_SendMessage(){
    if(win != undefined){
       // win.postMessage(data.value,'*');
       win.postMessage({'name':data.value},'*');
    }

}

//子页面接收数据
function onLoad(){
    window.addEventListener('message',function (e){
        //data.innerText = e.data;
        data.innerText = e.data.name;
        //alert(e.origin);
    })
}

//关闭子窗口返回数据
function onClick_close(){
    const win = remote.getCurrentWindow();
    ipcRenderer.send('close','窗口已经关闭');
    win.close();
}

//eval方法传递数据
function onClick_SendMessageEval(){
    if(win != undefined){
        win.eval('data.innerText = "'+ data.value + '"');
    }
}
