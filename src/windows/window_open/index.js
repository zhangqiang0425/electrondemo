//使用html5 api创建子窗口
/*
window.open方法
window.open(url[,title][,attributes]);
1.url:打开页面的链接(可以使本地链接，也可以是web链接)
2.title:设置要打开页面的标题，如果在页面中已经设置，则此参数被忽略
3.attributes:设置与窗口相关的一些属性，例如:宽度、高度
window.open方法的返回值
BrowserWindowProxy:可以认为是BrowserWindow的代理类

控制窗口
1.获取焦点:focus
2.失去焦点:blur
3.关闭窗口:close
4.显示打印对话框:print

窗口之间的交互:最简单的数据传递方式
B.postMessage(data,'*')

从子窗口返回数据给主窗口
ipcRenderer.send();
ipcMain.on();

页面来源

使用eval方法向子窗口传递数据
eval方法用来执行JavaScript代码

* */

const {app, BrowserWindow} = require('electron');
function createWindow () {
  win = new BrowserWindow({webPreferences:{
    nodeIntegration :true
    }});
  win.loadFile('index.html');
  //win.webContents.openDevTools();
  win.on('closed',()=>{
    console.log('closed');
    win == null;
  })
}
app.on('ready', createWindow);
app.on('window-all-closed', ()=>{
  console.log('window-all-closed');
  if(process.platform != 'darwin'){
    app.quit();
  }
});
app.on('activate',()=>{
  console.log('activate');
  if(win == null){
    createWindow();
  }
});
