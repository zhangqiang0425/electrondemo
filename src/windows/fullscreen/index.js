//窗口的尺寸和位置
/*
* width:窗口宽度
* height:窗口高度
*
* minWidth:窗口允许的最小宽度
* minHeight:窗口允许的最小高度
* maxWidth:窗口允许的最大宽度
* maxHeight:窗口允许的最大高度
*
* x:指定窗口的横坐标
* y:指定窗口的纵坐标
*
* 获取窗口尺寸
* getSize() 返回数组，[0]：width   [1]:height
* 设置窗口尺寸
* setSize(width,height,flag) flag：true,以动画效果改变尺寸(仅限于Mac Os)
* 获取窗口位置
* getPosition() 返回数组，[0]：x   [1]:y
* 设置窗口位置
* setPosition(x,y,flag) flag：true,以动画效果改变位置(仅限于Mac Os)
*
* 全屏窗口
* fullscreen:true
* 如果设置了width，height、x、y，系统会忽略这些属性，仍然是全屏显示
* maxWidth、maxHeight
*
* fullscreenable:false,在Mac OS X会阻止单击最大化按钮隐藏菜单
*
* setFullScreen方法可以动态将窗口设置为全屏状态，但fullscreenable属性值必须为true
*
* 通过win.isFullScreen()方法可以获取窗口是否为全屏
* */

const {app, BrowserWindow} = require('electron');
function createWindow () {
  win = new BrowserWindow({width:300,height:400});
  win.loadFile('index.html');
  win.setFullScreen(true);
  win.isFullScreen();
  win.webContents.openDevTools();
  win.on('closed',()=>{
    console.log('closed');
    win == null;
  })
}
app.on('ready', createWindow);
app.on('window-all-closed', ()=>{
  console.log('window-all-closed');
  if(process.platform != 'darwin'){
    app.quit();
  }
});
app.on('activate',()=>{
  console.log('activate');
  if(win == null){
    createWindow();
  }
});
