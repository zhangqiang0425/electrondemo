const remote = require('electron').remote;
const dialog = remote.dialog;
//打开最简单的文件框
function onClick_OpenFile(){
    const label = document.getElementById('label');
    label.innerText = dialog.showOpenDialog({properties:['openFile']});
}
//定制打开文件框
function onClick_CustomOpenFile(){
    const label = document.getElementById('label');
    var options = {};
    options.title = '打开文件';
    options.buttonLabel = '选择';
    options.properties = ['openFile'];
    options.defaultPath = 'D:\\360驱动大师目录';
    label.innerText = dialog.showOpenDialog(options);
}
//选择文件类型
function onClick_FileType(){
    const label = document.getElementById('label');
    var options = {};
    options.title = '打开文件';
    options.buttonLabel = '选择';
    options.properties = ['openFile'];
    options.filters = [
        {name:'图像文件',extensions:['jpg','png','bmp','gif']},
        {name:'视频文件',extensions:['avi','mp4','rm','rmvb']},
        {name:'音频文件',extensions:['mp3','wav']},
        {name:'所有文件(*.*)',extensions:['*']}
    ];
    label.innerText = dialog.showOpenDialog(options);
}

//选择目录
function onClick_Directory(){
    const label = document.getElementById('label');
    var options = {};
    options.title = '选择目录';
    options.buttonLabel = '选择目录';
    options.properties = ['openDirectory'];
    label.innerText = dialog.showOpenDialog(options);
}

//多选(文件和目录)
function onClick_multiSelections(){
    const label = document.getElementById('label');
    var options = {};
    options.title = '选择多个文件和目录';
    options.buttonLabel = '选择';
    options.properties = ['openFile','multiSelections'];
    //如果是Mac系统，添加openDirectory属性
    if(process.platform == 'darwin'){
        options.properties.push('openDirectory');
    }
    label.innerText = dialog.showOpenDialog(options);
}

//通过回调函数返回选择结果
function onClick_Callback(){
    const label = document.getElementById('label');
    var options = {};
    options.title = '选择多个文件和目录';
    options.buttonLabel = '选择';
    options.properties = ['openFile','multiSelections'];
    //如果是Mac系统，添加openDirectory属性
    if(process.platform == 'darwin'){
        options.properties.push('openDirectory');
    }
    dialog.showOpenDialog(options,(filePaths)=>{
        for(var i = 0; i < filePaths.length; i++){
            label.innerText += filePaths[i] + '\r\n'
        }
    });

}

