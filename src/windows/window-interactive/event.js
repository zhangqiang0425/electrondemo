const remote = require('electron').remote;
const BrowserWindow = remote.BrowserWindow;

//获取ipcMain对象
const ipcMain = remote.ipcMain;
const {ipcRenderer} = require('electron');
ipcMain.on('other',(event,str)=>{
    const laberReturn = document.getElementById('label_return');
    laberReturn.innerText = laberReturn.innerText + '\r\n'+ str;
});
//向新窗口传递数据
function onclick_SendData(){
    var win = new BrowserWindow({show:false,x:10,y:20,width:200,height:300,webPreferences : {
            nodeIntegration: true
        }});
    win.loadFile('other.html');
    win.once('ready-to-show',()=>{
        win.show();
        win.webContents.send('data',{name:'bill',salary:12345});
    });
}
//新窗口加载时显示接收的数据
function onLoad(){
    ipcRenderer.on('data',(event,obj)=>{
        const labelName = document.getElementById('label_name');
        const labelSalary = document.getElementById('label_salary');
        labelName.innerText = obj.name;
        labelSalary.innerText = obj.salary;
    })
}

//关闭新窗口将数据传递回来
function onClick_Close(){
    const win = remote.getCurrentWindow();
    ipcRenderer.send('other','窗口已经关闭');
    win.close();
}
