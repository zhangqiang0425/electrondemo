const remote = require('electron').remote;
const dialog = remote.dialog;
//显示简单的消息对话框
function onClick_MessageBox(){
    const label = document.getElementById('label');
    var options = {};
    options.title = '消息对话框';
    options.message = '这是一个消息对话框';
    label.innerText = dialog.showMessageBox(options);
}

//定制消息对话框图标
function onClick_MessageBoxIcon(){
    const label = document.getElementById('label');
    var options = {};
    options.title = '消息对话框';
    options.message = '定制消息对话框图标';
    options.icon = '..//..//..//image/12.png';
    label.innerText = dialog.showMessageBox(options);
}

//设置消息对话框类型
function onClick_MessageBoxType(){
    const label = document.getElementById('label');
    var options = {};
    options.title = '消息对话框';
    options.message = '设置消息对话框类型';
    options.type = 'warning';
    //options.icon = '..//..//..//image/12.png';
    label.innerText = dialog.showMessageBox(options);
}

//为消息对话框设置多个按钮
function onClick_MessageBoxButton(){
    const label = document.getElementById('label');
    var options = {};
    options.title = '消息对话框';
    options.message = '为消息对话框设置多个按钮';
    options.buttons = ['按钮1','按钮2','按钮3','按钮4','按钮5'];
    dialog.showMessageBox(options,(response)=>{
        label.innerText = '按了第' + (response + 1) + '个按钮'
    });
}