const remote = require('electron').remote;
const dialog = remote.dialog;

function onLoad(){
    const webview = document.getElementById('webview1');
    const loadstart = ()=>{
        console.log('loadstart');
    }
    const loadstop = ()=>{
        console.log('loadstop');
    }

    webview.addEventListener('did-start-loading',loadstart);
    webview.addEventListener('did-stop-loading',loadstop)
}
