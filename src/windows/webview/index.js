/*
在窗口中嵌入web页面
1.<webview>标签
2.webview事件
3.在<webview>中装载页面中执行Node.js API
4.webview常用的API
* */

const {app, BrowserWindow} = require('electron');
function createWindow () {
  win = new BrowserWindow({webPreferences:{
    nodeIntegration :true
    }});
  win.loadFile('index.html');
  //win.webContents.openDevTools();
  win.on('closed',()=>{
    console.log('closed');
    win == null;
  })
}
app.on('ready', createWindow);
app.on('window-all-closed', ()=>{
  console.log('window-all-closed');
  if(process.platform != 'darwin'){
    app.quit();
  }
});
app.on('activate',()=>{
  console.log('activate');
  if(win == null){
    createWindow();
  }
});
